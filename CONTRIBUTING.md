By joining this chat, you automatically agree to follow and abide by these rules. If any user has inquiries regarding anything stated here, voice any opinions or suggestions to administrators listed on the side-bar.

The views, thoughts, and opinions expressed by the users of this chat are their own and do not represent the views and opinions of the Owners, it's employees, or subsidiaries.

This Server is not presented AS-IS, rules that are neither expressed nor implied in this document may be enforced at any time.

The primary function of this chat is for the use of familiars and acquaintances of Matthew Murray. You may be removed at any time, for any reason.

The Owner of this chat is not represented or endorsed by the Ford Motor Company, or it's subsidiaries.

All chats are subject to these rules:
 
 - You must be at least thirteen (13) years of age to use this chat as per COPPA guidelines.
 - Respect others in chat, regardless of how they may treat you. Personal disputes must be handled on a personal level. 
 - Many users may not have a filter, and are not expected to, but there is a limit. Use common sense. 
 - While all users have administrator access, the Owner still has seniority over all members.
 - When a voice chat reaches full capacity, move to another chat.
 - Make a conscience effort to post original content.
 - At any point, Administrators may break listed rules for their own amusement. This is not an invitation for any user to do the same.

    Edits may be made to these rules spontaneously and without warning. Exceptions to most rules will be made at administrator discretion. Clarification of any rules may be requested at any time. Failure to comply with any of these rules will result in a temporary mute, change of permissions, or removal from this server.
...    

#general -

   - This chat is explicitly for regular discussion. Off-topic discussions are allowed, but refer to the corresponding sub-chats if applicable.

#music -

   - This chat is for music and soundcloud links, and the use of the music bot. Off-topic discussions are not allowed. All genres are allowed.

#art -

   - This chat is for posting original art of any nature, explicit or otherwise, and for assistance and critique. 

#nsfw -

    - This chat is explicitly for anything pornographic in nature. The following is restricted from this chat:

      •    Extreme fetishes are not allowed. This includes, but is not limited to: Guro, Scat, Digestion Vore, or anything of the sort. Categories may be added or removed.
      •    Make an effort to post original content, as in anything not already in the chat.
      •    Discussion involving the topic at hand is allowed, keep normal conversation in the proper channels.
      •    Joke content is absolutely, without question, not allowed